/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
  FlatList
} from 'react-native';


const App = () => {

  const [firstDfa, setFirstDfa] = useState()
  const [secondDfa, setSecondDfa] = useState()

  const [changePager, setChangePager] = useState(0)

  const [ejtema, setEjtema] = useState([])
  const [fin, setFin] = useState([])

  function findTheResualt(input, searcher) {
    // console.log(searcher)
    for (var i = 0; i < input.length; i++) {
      if (input[i].split(",")[0] == searcher) {
        return (input[i].split(",")[1])
      }
    }
  }

  function deleteFakeState(input) {
    //  console.log(input[0].split('{').pop().split('}')[0])

    var rangedArray = []
    var counter = 0
    for (var i = 0; i < input.length; i++) {
      for (var j = 0; j < input.length; j++) {
        if (input[i].split('{').pop().split('}')[0] == input[j].split('(').pop().split(')')[0] || input[i].split('{').pop().split('}')[0] == input[0].split('{').pop().split('}')[0]) {
          rangedArray[counter] = input[i]
          counter++;
        }
      }
    }
    rangedArray = [...new Set(rangedArray)];
    return (rangedArray)

  }
  function deleteDuplicate(input) {
    var equavalnce = input
    for (var i = 0; i < equavalnce.length; i++) {
      equavalnce[i] = equavalnce[i].replace(/\s/g, '')
      var helper = equavalnce[i].split(",")
      helper = [...new Set(helper)];
      var St = ""
      for (var j = 0; j < helper.length; j++) {
        if (St == "") {
          St = helper[j]
        } else {
          St = St + "," + helper[j]
        }
      }
      equavalnce[i] = St
    }
    return equavalnce
  }

  function findGoWhere(input, str) {
    var helpForCount = ""

    for (var k = 0; k < input.length; k++) {
      if (input[k].split("->")[0] == str) {
        if (helpForCount == "") {
          helpForCount = input[k].split("->")[1].charAt(0) + "{" + input[k].split('(').pop().split(')')[0] + "}"
        } else {
          helpForCount = helpForCount + "," + input[k].split("->")[1].charAt(0) + "{" + input[k].split('(').pop().split(')')[0] + "}"
        }
      }
    }
    // console.log(input)
    // console.log(helpForCount)
    return helpForCount
  }

  // function allEqual(arr) {
  //   return new Set(arr).size == 1;
  // }

  function equavalnceMoratabKon(input) {
    var equavalnce = []
    var mainEquavalnce = []
    var counter = 0
    for (var i = 0; i < input.length; i++) {
      equavalnce[i] = [...new Set(input[i].split(","))];
    }
    // console.log(equavalnce)
    for (var i = 0; i < equavalnce.length; i++) {
      var help = 1
      for (var j = 0; j < equavalnce.length; j++) {
        if (i != 0) {
          if (i == j) break
        }
        for (var k = 0; k < equavalnce[j].length; k++) {
          if (equavalnce[j][k].includes(equavalnce[i])) {
            help = 0
          }
        }
      }
      if (help) {
        mainEquavalnce[counter] = equavalnce[i]
        counter++
      }
    }
    console.log(mainEquavalnce)
    return mainEquavalnce
  }

  function dfaCreator(input, equavalnce) {
    // console.log(equavalnce[0].join())
    var finalFinalDfa = []
    var counter = 0
    for (var i = 0; i < equavalnce.length; i++) {
      for (var k = 0; k < input.length; k++) {
        if (equavalnce[i].join().split(",")[0] == input[k].split("->")[0]) {
          finalFinalDfa[counter] = input[k]
          counter++
        }
      }
    }
    console.log(finalFinalDfa)
    setFin(finalFinalDfa)
  }



  function loopEquavalance(input, equavalnce, alphabet) {
    console.log(equavalnce)
    var equavalnceHelper = []
    var equavalnceHelperChecker = []
    var counter = 0;
    var looper = 1
    while (looper) {
      for (var i = 0; i < equavalnce.length; i++) {
        var goToState = []

        var str = equavalnce[i].split(",")
        for (var j = 0; j < str.length; j++) {
          goToState[j] = findGoWhere(input, str[j])

        }
        for (var j = 0; j < goToState.length; j++) {
          var stringArray = goToState[j].split(",")
          for (var k = j; k < goToState.length; k++) {
            var helpCount = 0
            for (var z = 0; z < stringArray.length; z++) {
              if (goToState[k].includes(stringArray[z])) {
                helpCount++
              }
            }
            if (helpCount == stringArray.length) {
              if (equavalnceHelper[counter] == undefined) {
                equavalnceHelper[counter] = str[j] + "," + str[k]
                counter++
              } else {
                equavalnceHelper[counter] = equavalnceHelper + "," + str[j] + "," + str[k]
                counter++
              }
            }
          }
        }
      }
      // console.log("|||||||||||||||||||||||||||||||||||||||||||||")
      // console.log(equavalnceHelper)
      equavalnceHelper = equavalnceMoratabKon(equavalnceHelper)
      dfaCreator(input, equavalnceHelper)
      looper = 0
    }
  }

  function dfaMinimization(input, alphabet) {
    var equavalnce = []
    var counter = 0
    for (var i = 0; i < input.length; i++) {
      if (!input[i].split("->")[0].includes("Q")) {
        if (equavalnce[counter] == undefined) {
          equavalnce[counter] = input[i].split("->")[0]
        }
        equavalnce[counter] = equavalnce[counter] + ", " + input[i].split("->")[0]
      } else {
        if (equavalnce[counter + 1] == undefined) {
          equavalnce[counter + 1] = input[i].split("->")[0]
        }
        equavalnce[counter + 1] = equavalnce[counter + 1] + ", " + input[i].split("->")[0]
      }
    }
    equavalnce = deleteDuplicate(equavalnce)
    // console.log(equavalnce)
    // findGoWhere(input , "{A}")
    loopEquavalance(input, equavalnce, alphabet)

  }

  function recognizeDfaEjtema() {
    var str1 = firstDfa.replace(/\s/g, '')
    var strArr1 = str1.split("|")
    var str2 = secondDfa.replace(/\s/g, '')
    var strArr2 = str2.split("|")
    // console.log(strArr1)
    var finalArrState = []
    var alphabet = []
    var k = 0;
    var z = 0;
    for (var i = 0; i < strArr1.length; i++) {
      alphabet[z] = strArr1[i].split("->")[1].split(",")[0]
      z++;
      for (var j = 0; j < strArr2.length; j++) {
        finalArrState[k] = strArr1[i].split("->")[0] + "," + strArr2[j].split("->")[0]
        alphabet[z] = strArr2[j].split("->")[1].split(",")[0]
        z++;
        k++
      }
    }
    var finalArr = []
    var counter = 0
    finalArrState = [...new Set(finalArrState)];
    alphabet = [...new Set(alphabet)];
    console.log(alphabet)
    for (var a = 0; a < alphabet.length; a++) {
      for (var i = 0; i < finalArrState.length; i++) {
        var searcher = finalArrState[i].split(",")[0] + "->" + alphabet[a]
        // console.log(searcher)
        var first = findTheResualt(strArr1, searcher)
        var second = findTheResualt(strArr2, finalArrState[i].split(",")[1] + "->" + alphabet[a])
        finalArr[counter] = "{" + finalArrState[i] + "}" + "->" + alphabet[a] + "," + "(" + first + "," + second + ")"
        counter++
      }
    }

    // console.log(finalArr)
    // console.log("*********************************")
    finalArr = deleteFakeState(finalArr)
    setEjtema(finalArr)
    setChangePager(1)
    // console.log(finalArr)
    var test = ["{A}->0,(B)", "{A}->1,(C)", "{B}->0,(B)", "{B}->1,(D)", "{C}->1,(C)", "{C}->0,(B)", "{D}->0,(B)", "{D}->1,(Q)", "{Q}->1,(C)", "{Q}->0,(B)"]
    dfaMinimization(finalArr, alphabet)

  }

  function alKon() {
    if(fin == "") {
      alert(ejtema)
    }else {
      alert(fin)
    }
    
  }

  const renderItem = ({ item }) => {


    return (
      <Text>{item}</Text>
    );
  };

  if (changePager == 0) {
    return (
      <View style={styles.container}>

        <View style={styles.littleContainer}>
          <TextInput style={{ color: "#fff" }} placeholder="write your first dfa" multiline={true} onChangeText={(x) => setFirstDfa(x)} ></TextInput>
        </View>


        <View style={styles.littleContainer}>
          <TextInput style={{ color: "#fff" }} placeholder="write your second dfa" multiline={true} onChangeText={(x) => setSecondDfa(x)} ></TextInput>
        </View>

        <TouchableOpacity onPress={() => recognizeDfaEjtema()} style={{
          backgroundColor: "#adadad", width: "20%", height: "8%", borderRadius: 20, justifyContent: "center",
          alignItems: "center", marginVertical: "10%", marginHorizontal: "40%"
        }} >
          <Text>Start</Text>
        </TouchableOpacity>



        <TouchableOpacity onPress={() => alert("با سلام 👋 نکته اولی که باید خدمت شما ذکر کنم این است که وقتی میخواهید dfa را درون فرم ها بنویسید حتما توجه کنید که به صورت A -> 1,B باشددر ضمن بعد از هر خط dfa یادتان باشد یک |بگذارید  به جز خط اخر که نیازی ندارد دو نکته آخر اینکه حتما حالاتی که نهایی هستن را با Q نشان دهید که از Q0 تا هر چه قدر بخواهید میتوانید عدد گذاری کنید و حرف آخر اینکه حتما حالت شروع را در خط اول وارد کنید")} style={{
          backgroundColor: "#adadad", width: "20%", height: "8%", borderRadius: 20, justifyContent: "center",
          alignItems: "center", marginVertical: "10%", marginHorizontal: "40%"
        }} >
          <Text>Readme</Text>
        </TouchableOpacity>



      </View>
    );
  } else if (changePager == 1) {
    return (
      <View style={styles.container}>

        <View style={styles.littleContainer}>
          {/* <Text style={{ color: "#fff" }} multiline={true} >{ejtema}</Text> */}
          <FlatList
            data={ejtema}
            renderItem={renderItem}
          />
          
        </View>

        <TouchableOpacity onPress={() => alKon()} style={{
          backgroundColor: "#adadad", width: "20%", height: "8%", borderRadius: 20, justifyContent: "center",
          alignItems: "center", marginVertical: "10%", marginHorizontal: "40%"
        }} >
          <Text>see</Text>
        </TouchableOpacity>

      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#2d2d2d",
    width: "100%",
    height: "100%"
  },
  littleContainer: {
    backgroundColor: "#9d9d9d",
    height: "8%",
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "10%",
    marginVertical: "10%",
    borderRadius: 20
  },
  rowCenter: {
    flexDirection: 'row-reverse',
    justifyContent: 'center',
  },
  rowSpace: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowSpaceReverse: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },
  rowStart: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  rowStartReverse: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
  },
  rowEnd: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

export default App;
